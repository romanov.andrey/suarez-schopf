import matplotlib.pyplot as plt

from jitcdde import jitcdde, y, dy, t, jitcdde_lyap
from symengine import sin
import numpy as np
from scipy.stats import sem

history1 = [5 + 0.00001]

lyapunov_exponents = []
for A in np.arange(0.068, 0.075, 0.0001):
    print(A)

    # parameters of the Suarez-Schopf model
    tau = 1.596
    alpha = 0.75


    def g(y):
        return (y) ** 3


    f = {
        y(i, t): y(i, t) - alpha * y(i, t - tau) - g(y(i, t)) + A * sin(t)

        for i in range(1)
    }

    # step size in time at which the solution is returned by the integrator
    step = 0.01

    # max step for the integrator
    max_step = 0.0001

    # Time up to which the starting trajectory needs to be integrated
    # Then we use the final state of the integrated to start integrating linearized system
    finish = 2 * np.pi * 1500

    delay_in_steps = int(np.ceil(tau / step))
    two_pi_in_steps = int(np.ceil(2 * np.pi / step))

    DDE = jitcdde(f, max_delay=tau)
    DDE.constant_past([5 + 0.00001])
    DDE.set_integration_parameters(max_step=max_step, first_step=max_step, atol=1e-6, rtol=1e-6)
    DDE.adjust_diff()

    ts, ss = [], []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state[0])

    ts = np.array(ts)
    ss = np.array(ss)
    _ts = (ts - ts[-1])[1:]
    dts = np.diff(_ts)
    dx = np.diff(ss[1:]) / dts
    N = len(dts)

    # how much of Lyapunov exponents to compute
    n_lyap = 2

    DDE_lyap = jitcdde_lyap(f, n_lyap=n_lyap, max_delay=tau)
    for i in range(N - delay_in_steps, N):
        DDE_lyap.add_past_point(ts[i], ss[i], dx[i])
    DDE_lyap.set_integration_parameters(max_step=max_step, first_step=max_step, atol=1e-6, rtol=1e-6)
    DDE_lyap.adjust_diff()

    T = ts[-1]
    ts, ss, ls, ws = [], [], [], []

    # time upto which solutions of the linearized system are computed
    finish = 5_000

    for time in np.arange(T + step, T + finish, step):
        ts.append(time)
        state, lyap, weight = DDE_lyap.integrate(time)
        ss.append(state)
        ls.append(lyap)
        ws.append(weight)

    ls = np.array(ls)
    ws = np.array(ws)

    ans = []
    for i in range(n_lyap):
        L = np.average(ls[400:, i], weights=ws[400:])
        stderr = sem(ls[400:, i])
        print("%i. Lyapunov exponent: % .4f +/- %.4f" % (i + 1, L, stderr))
        ans.append(L)
    lyapunov_exponents.append(ans)

    ss = np.array(ss)
    yy = ss
    fig, ax = plt.subplots(1, figsize=(4, 4))
    ax.tick_params(labelsize=15)
    plt.scatter(yy[delay_in_steps:], yy[:-delay_in_steps], color='red', s=2)
    plt.xlim([-1, 1])
    plt.ylim([-1, 1])
    plt.savefig(f'amp_{round(A, 4)}.png', dpi=400)

# To plot the Lyapunov exponents use the printed array in PlotLyapunovExponents.py
print(lyapunov_exponents)
