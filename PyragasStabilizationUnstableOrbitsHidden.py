from jitcdde import jitcdde, y, dy, t
from symengine import sign
import matplotlib as mpl
import numpy as np


def GetStabilizedOrbit():
    # Tolerance for numerical integration
    TOL = 1e-6

    # an estimate for the period of unstable orbit
    T = 28.7299

    # parameters tau,alpha of the Suarez-Schopf model
    tau = 1.58
    alpha = 0.75

    # time steps at which solution is obtained
    step = 0.01

    # max step in time for integration
    max_step = 1e-3

    # the time upto which solutions are obtained
    finish = 30 * T

    # constants for initial condtions
    epsL = 0.03535
    m = 1.

    def history1(t):
        return (epsL * (m + 1) / tau) * t + epsL

    def g(y):
        return (y) ** 3

    f = {
        y(i, t): y(i, t) - alpha * y(i, t - tau) - g(y(i, t)) - 1000 * (1. + sign(t - T)) * (y(i, t) - y(i, t - T))
        for i in range(1)
    }

    DDE = jitcdde(f, max_delay=T)
    DDE.past_from_function(history1)
    DDE.set_integration_parameters(max_step=max_step, first_step=max_step, atol=TOL, rtol=TOL)
    DDE.adjust_diff()

    ts, ss = [], []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state)
    ss = np.array(ss)
    return ts, ss
