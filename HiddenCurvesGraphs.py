from pylab import linspace, subplots, show
import numpy as np
import matplotlib.pyplot as plt


def f(alpha):
    return np.arccos((3 * alpha - 2) / alpha) / np.sqrt(alpha ** 2 - (3 * alpha - 2) ** 2)


def g(mu):
    return np.arccos((3 * alpha0 + mu - 2) / alpha0) / np.sqrt(alpha0 ** 2 - (3 * alpha0 + mu - 2) ** 2)


alpha_min = 0.5245
alpha_max = 0.99
mu_min = 0.001
mu_max = 0.49
alpha0 = 0.75
step = 0.001
alphas = np.arange(alpha_min, alpha_max, step)
taus = f(alphas)

mus = np.arange(mu_min, mu_max, step)
taus2 = g(mus)

phi1 = 0.042
A_Shift1 = 0.000
alphasROT1 = -np.sin(phi1) * (taus - 1) + np.cos(phi1) * (alphas - 1) + 1 - A_Shift1
tausROT1 = np.cos(phi1) * (taus - 1) + np.sin(phi1) * (alphas - 1) + 1

phi2 = 0.006
A_Shift2 = 0.055
alphasROT2 = -np.sin(phi2) * (taus - 1) + np.cos(phi2) * (alphas - 1) + 1 - A_Shift2
tausROT2 = np.cos(phi2) * (taus - 1) + np.sin(phi2) * (alphas - 1) + 1

alphasH = [
    0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.50, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.60, 0.61, 0.62,
    0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.70, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.80, 0.81, 0.82,
    0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.90, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99
]
tausR = [
    8.3, 7.24, 6.43, 5.80, 5.28, 4.86, 4.5, 4.19, 3.92, 3.66, 3.48, 3.33, 3.16, 2.99, 2.85, 2.73, 2.62, 2.51, 2.41, 2.33,
    2.25, 2.17, 2.10, 2.04, 1.97, 1.92, 1.86, 1.81, 1.76, 1.72, 1.67, 1.63, 1.59, 1.56, 1.52, 1.49, 1.46, 1.43, 1.40, 1.37,
    1.34, 1.32, 1.29, 1.27, 1.24, 1.22, 1.20, 1.177, 1.157, 1.137, 1.118, 1.10, 1.082, 1.063, 1.047, 1.03, 1.01
]

tausL = [
    6.18, 5.65, 5.19, 4.815, 4.485, 4.20, 3.95, 3.725, 3.53, 3.35, 3.19, 3.04, 2.91, 2.79, 2.68, 2.58, 2.48, 2.39, 2.31,
    2.235, 2.165, 2.10, 2.035, 1.98, 1.92, 1.87, 1.82, 1.775, 1.73, 1.69, 1.65, 1.61, 1.57, 1.54, 1.51, 1.48, 1.45, 1.42,
    1.39, 1.36, 1.34, 1.32, 1.29, 1.27, 1.24, 1.22, 1.20, 1.177, 1.157, 1.137, 1.118, 1.10, 1.082, 1.063, 1.047, 1.03, 1.01
]

fig, ax = subplots(1, figsize=(4, 4))

plt.rcParams['text.usetex'] = True
ax.set_xlabel(r"$\tau$", fontsize=20)
ax.set_ylabel(r"$\alpha$", fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)
ax.grid()
ax.set(xlim=[0.9, 6.17])
ax.plot(taus, alphas)
ax.plot(tausR, alphasH)
ax.plot(tausL, alphasH)

plt.show()
