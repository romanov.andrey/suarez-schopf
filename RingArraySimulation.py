from jitcdde import jitcdde, y, dy, t
from symengine import cos, sin, exp
import numpy as np
import matplotlib.pyplot as plt

step = 0.01
step_N = 100
tau = 2.4
finish = 1000
lasttime = 500

d = 0.01

a = -1.
q = 0.01
b = 60.

n = 2

def D(y, i): 
    return y(i % n, t) - q * y(i % n, t - tau)

def g(y):
    return (y)**3
    
history11 = [
    1*(4*cos(t)+4), 
    1*(-3*exp(t) + 3)
]

history12 = [
    -1*(4*cos(t)+4), 
    -1*(-3*exp(t) + 3)
]

history13 = [
    1*(-3*exp(t) + 3),
    1*(4*cos(t)+4)
]

history14 = [
    -1*(-3*exp(t) + 3),
    -1*(4*cos(t)+4), 
    
]

history15 = [
    1*(4*cos(t)+4),
    1*(4*cos(t)+4), 
]

history2 = [
    -0.001*cos(t), 
    +0.001*sin(t)
]

f = {
    y(i, t): -a * y(i, t) - b * q * y(i, t - tau) - g(y(i, t)) + q * g(y(i, t - tau)) +
              d * (D(y, i+1) - 2 * D(y, i) + D(y, i-1)) + 
              q *dy(i, t - tau)
    
    for i in range(n)
}

def get_solution(DDE, history):
    DDE.past_from_function(history)
    DDE.set_integration_parameters(max_step=step, first_step=step, atol=1e-5, rtol=1e-5)
    DDE.adjust_diff()

    ss = []
    ts = []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state)
    ss = np.array(ss)
    return ts, ss

DDE = jitcdde(f, max_delay=tau)
ts11, ss11 = get_solution(DDE=DDE, history=history11)
ts2, ss2 = get_solution(DDE=DDE, history=history2)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.clear()
ax.set_title(f'a={a}, b = {b}, q = {q}, d = {d}')
ax.set_xlabel('x1(t)')
ax.set_ylabel('x1(t-tau)')
ax.set_zlabel('x2(t)')
    
ax.plot(
    ss11[int(np.floor(tau/step)):,0][-(finish-lasttime)*step_N:], 
    ss11[:-int(np.floor(tau/step)),0][-(finish-lasttime)*step_N:], 
    ss11[int(np.floor(tau/step)):,1][-(finish-lasttime)*step_N:],
    label="per1"
)

ax.plot(
    -ss11[int(np.floor(tau/step)):,0][-(finish-lasttime)*step_N:], 
    -ss11[:-int(np.floor(tau/step)),0][-(finish-lasttime)*step_N:], 
    -ss11[int(np.floor(tau/step)):,1][-(finish-lasttime)*step_N:],
    label="per2"
)

ax.plot(
    ss2[int(np.floor(tau/step)):,0], 
    ss2[:-int(np.floor(tau/step)),0], 
    ss2[int(np.floor(tau/step)):,1],
    label="equal1"
)

ax.plot(
    -ss2[int(np.floor(tau/step)):,0], 
    -ss2[:-int(np.floor(tau/step)),0], 
    -ss2[int(np.floor(tau/step)):,1],
    label="equal2"
)

ax.plot(
    ss2[int(np.floor(tau/step)):,1], 
    ss2[:-int(np.floor(tau/step)),1], 
    ss2[int(np.floor(tau/step)):,0],
    label="equal3"
)

ax.plot(
    -ss2[int(np.floor(tau/step)):,1], 
    -ss2[:-int(np.floor(tau/step)),1], 
    -ss2[int(np.floor(tau/step)):,0],
    label="equal3"
)

plt.show()
