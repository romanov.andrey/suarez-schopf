from pylab import linspace, subplots, show
from jitcdde import jitcdde, y, dy, t
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate as integ
from PyragasStabilizationUnstableOrbitsHidden import GetStabilizedOrbit

# the parameters tau and alpha from the Suarez-Schopf model
tau = 1.58
alpha = 0.75

# the first two roots (always real) of 1 - alpha*exp(-tau*p) - p = 0 for the above given parameters tau and alpha
# needs to compute the projector on (c1,c2)-coordinates
lam1 = 0.781994
lam2 = -0.390913

# time steps at which solution is obtained
step = 0.01
# Tolerance for numerical integration
TOL = 1e-6

delay_in_steps = int(tau / step)
step_T = 100

# max step in time for integration
maxstep = 1e-3

trans_T = 0

# the time upto which solutions are obtained
finish = 300.

# constants for initial condtions
eps = 0.01
epsL = 0.03535
m = 1.

history12 = [
    -((epsL * (m + 1) / tau) * t + epsL)
]


def hist12(t):
    return -((epsL * (m + 1) / tau) * t + epsL)


history13 = [
    (10. * (epsL * (m + 1) / tau) * t + 10. * epsL)
]


def hist13(t):
    return (10. * (epsL * (m + 1) / tau) * t + 10. * epsL)


history01 = [
    eps
]


def hist01(t):
    return eps


history02 = [
    -eps
]


def hist02(t):
    return -eps


history03 = [
    (eps * (m + 1) / tau) * t + eps
]


def hist03(t):
    return (eps * (m + 1) / tau) * t + eps


history04 = [
    -((eps * (m + 1) / tau) * t + eps)
]


def hist04(t):
    return -((eps * (m + 1) / tau) * t + eps)


def Coord(yy, lam):
    ans = []
    x = np.array([-tau + step * i for i in range(delay_in_steps)])
    for i in range(delay_in_steps, len(yy)):
        a = -alpha * np.exp(-lam * tau - lam * x)
        b = yy[i - delay_in_steps:i]
        y = a * b
        per = -(integ.simpson(y, x) + yy[i - 1]) / (alpha * tau * np.exp(-tau * lam) - 1)
        ans.append(per)
    return np.array(ans)


def g(y):
    return (y) ** 3


f = {
    y(i, t): y(i, t) - alpha * y(i, t - tau) - g(y(i, t))

    for i in range(1)
}


def get_solution(DDE, history):
    DDE.past_from_function(history)
    DDE.set_integration_parameters(max_step=maxstep, first_step=maxstep, atol=TOL, rtol=TOL)
    DDE.adjust_diff()

    ss = []
    ts = []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state)
    ss = np.array(ss)
    return ts, ss


tt = np.arange(0, finish, step=step)
fig, ax = subplots(1, figsize=(4, 4))

DDE = jitcdde(f, max_delay=tau)
# computing the trajectory localizing the hidden orbit from interior
ts11, ss11 = get_solution(DDE=DDE, history=history12)
# computing the trajectory localizing the hidden orbit from exterior
ts12, ss12 = get_solution(DDE=DDE, history=history13)

# computing the trajectories near 0
ts01, ss01 = get_solution(DDE=DDE, history=history01)
ts02, ss02 = get_solution(DDE=DDE, history=history02)
ts03, ss03 = get_solution(DDE=DDE, history=history03)
ts04, ss04 = get_solution(DDE=DDE, history=history04)

# computing the unstable periodic orbit via Pyragas stabilization
ts05, ss05 = GetStabilizedOrbit()

yy11 = []
yy11.extend(ss11[:, 0])

yy12 = []
yy12.extend(ss11[:, 0])
# removing a transient time for the trajectory localizing the hidden orbit
yy12 = yy12[int(1. / step) * 200:]

yy13 = []
yy13.extend(ss12[:, 0])

yy01 = []
# adding the initial data for the trajectory
for i in reversed(range(0, delay_in_steps)):
    yy01.append(hist01(-i * step))
yy01.extend(ss01[:, 0])

yy02 = []
# adding the initial data for the trajectory
for i in reversed(range(0, delay_in_steps)):
    yy02.append(hist02(-i * step))
yy02.extend(ss02[:, 0])

yy03 = []
# adding the initial data for the trajectory
for i in reversed(range(0, delay_in_steps)):
    yy03.append(hist03(-i * step))
yy03.extend(ss03[:, 0])

yy04 = []
# adding the initial data for the trajectory
for i in reversed(range(0, delay_in_steps)):
    yy04.append(hist04(-i * step))
yy04.extend(ss04[:, 0])

yy05 = []
yy05.extend(ss05[:, 0])

yy11 = np.array(yy11)
yy12 = np.array(yy12)
yy01 = np.array(yy01)
yy02 = np.array(yy02)
yy03 = np.array(yy03)
yy04 = np.array(yy04)
yy05 = np.array(yy05)
# removing the first tau*100 seconds from the stabilized orbit as a transient process
yy05 = yy05[delay_in_steps * 100:]

# Computing the coordinates (c1,c2) for each solution
c111 = Coord(yy11, lam1)
c112 = Coord(yy11, lam2)

c121 = Coord(yy12, lam1)
c122 = Coord(yy12, lam2)

c131 = Coord(yy13, lam1)
c132 = Coord(yy13, lam2)

c011 = Coord(yy01, lam1)
c012 = Coord(yy01, lam2)

c021 = Coord(yy02, lam1)
c022 = Coord(yy02, lam2)

c031 = Coord(yy03, lam1)
c032 = Coord(yy03, lam2)

c041 = Coord(yy04, lam1)
c042 = Coord(yy04, lam2)

c051 = Coord(yy05, lam1)
c052 = Coord(yy05, lam2)

plt.rcParams['text.usetex'] = True
# ax.grid(True)
ax.set_xlabel("$С_1$", fontsize=20)
ax.set_ylabel("$С_2$", fontsize=20)
ax.tick_params(labelsize=20)
# ax.set_aspect('equal')

# plotting the projected trajectories
ax.plot(c111, c112, c='tab:orange')
ax.plot(c011, c012, c='tab:green')
ax.plot(c021, c022, c='tab:brown')
ax.plot(c031, c032, c='tab:purple')
ax.plot(c041, c042, c='tab:pink')
ax.plot(c051, c052, c='tab:red')
ax.plot(c131, c132, c='tab:gray')
ax.plot(c121, c122, c='tab:cyan')

plt.show()
