from pylab import linspace, subplots, show
from jitcdde import jitcdde, y, dy, t
from symengine import sin
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate as integ

# import matplotlib
# matplotlib.use("svg")

mpl.rcParams['legend.fontsize'] = 10

history1 = [
    5
]

history2 = [
    5 + 0.00001
]

# parameters for the Suarez-Schopf model
tau = 1.596
alpha = 0.75

# the first two solutions (always real) to 1 - alpha e^{-tau p} - p = 0
lam1 = 0.786117
lam2 = -0.383766

# time step at which the solution will be returned by the integrator
step = 0.01
# max step for the integrator
maxstep = 0.0001

# step_T = 100

# transition time times 2\Pi to be removed in plotting
trans_PT = 300

# time up to which the solution will be obtained (we used 100000 to plot the attractor since we plot for times 2pik, but this requires time to compute)
finish = 100000

delay_in_steps = int(tau / step)


def g(y):
    return (y) ** 3


def Coord(yy, lam):
    ans = []
    x = np.array([-tau + step * i for i in range(delay_in_steps)])
    for i in range(delay_in_steps, len(yy)):
        a = -alpha * np.exp(-lam * tau - lam * x)
        b = yy[i - delay_in_steps:i, 0]
        y = a * b
        per = -(integ.simpson(y, x) + yy[i - 1]) / (alpha * tau * np.exp(-tau * lam) - 1)
        ans.append(per)
    return np.array(ans)


def CoordK(yy, lam, k):
    ans = []
    x = np.array([-tau + step * i for i in range(delay_in_steps)])
    for i in range(int(delay_in_steps / k) + 1, int(len(yy) / k)):
        a = -alpha * np.exp(-lam * tau - lam * x)
        j = i * k
        b = yy[j - delay_in_steps:j, 0]
        y = a * b
        per = -(integ.simpson(y, x) + yy[j - 1]) / (alpha * tau * np.exp(-tau * lam) - 1)
        ans.append(per)
    return np.array(ans)


f = {
    y(i, t): y(i, t) - alpha * y(i, t - tau) - g(y(i, t)) + 0.073 * sin(t)

    for i in range(1)
}


def get_solution(DDE, history):
    DDE.past_from_function(history)
    DDE.set_integration_parameters(max_step=maxstep, first_step=maxstep, atol=1e-6, rtol=1e-6)
    DDE.adjust_diff()

    ss = []
    ts = []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state)
    ss = np.array(ss)
    return ts, ss


tt = np.arange(0, finish, step=step)
fig, ax = subplots(1, figsize=(4, 4))

DDE = jitcdde(f, max_delay=tau)

ts11, ss11 = get_solution(DDE=DDE, history=history1)
ts12, ss12 = get_solution(DDE=DDE, history=history2)

two_pi_in_steps = int(2 * np.pi / step)
yy11 = np.array(ss11[two_pi_in_steps * trans_PT:])
yy12 = np.array(ss12[two_pi_in_steps * trans_PT:])

ax.tick_params(labelsize=15)
c11 = CoordK(yy11, lam1, two_pi_in_steps)
c12 = CoordK(yy11, lam2, two_pi_in_steps)
c21 = CoordK(yy12, lam1, two_pi_in_steps)
c22 = CoordK(yy12, lam2, two_pi_in_steps)
c11P = c11
c12P = c12
c21P = c21
c22P = c22
plt.tick_params(labelsize=15)
plt.scatter(c11P, c12P, color='red', s=2)
plt.scatter(c21P, c22P, s=2)

plt.show()

diff = ss11 - ss12

plt.plot(ts11, diff)
plt.show()
